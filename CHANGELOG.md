# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.8](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.7...v0.1.8) (2020-03-22)


### Bug Fixes

* small screen size adjustments ([deb734f](https://gitlab.com/wirvsvirus-410/frontend/commit/deb734f9ee3672baa075221692f0a6c2d5d6d1f1))

### [0.1.7](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.6...v0.1.7) (2020-03-22)


### Features

* disclaimer and footer informations ([d760e87](https://gitlab.com/wirvsvirus-410/frontend/commit/d760e8742be37ce356ae7fb43f7cf71e1ec4159a))

### [0.1.6](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.5...v0.1.6) (2020-03-22)


### Features

* searchbox connected with map ([e2ce0ca](https://gitlab.com/wirvsvirus-410/frontend/commit/e2ce0ca3a420c4b25847593d4c3d4a3e8a887ded))


### Bug Fixes

* cp ([94c9688](https://gitlab.com/wirvsvirus-410/frontend/commit/94c9688e724ce8d1bbd0f75d6a86e0bc5092f938))

### [0.1.5](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.4...v0.1.5) (2020-03-22)


### Bug Fixes

* handle click on cluster correctly ([dde0b62](https://gitlab.com/wirvsvirus-410/frontend/commit/dde0b62ee3ccad96589382bd85440d8029bac577))

### [0.1.4](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.3...v0.1.4) (2020-03-22)


### Features

* added hospitalName layer ([312fa6f](https://gitlab.com/wirvsvirus-410/frontend/commit/312fa6fce275d5d4ee6bce5632985e41599b1234))

### [0.1.3](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.2...v0.1.3) (2020-03-22)


### Features

* added capacityType control implementation ([1f52233](https://gitlab.com/wirvsvirus-410/frontend/commit/1f5223343dffce290e5d3a3ac62a98dbf877fbac))
* added control implementations for day and toggle ([a3abc9c](https://gitlab.com/wirvsvirus-410/frontend/commit/a3abc9c4aea65380522f5cf86af3fec7e382db90))
* added prediction data ([11cd12b](https://gitlab.com/wirvsvirus-410/frontend/commit/11cd12bce50eebe1b52fb6bf2901a0c07eb58252))
* added ui for control-panel filtering ([152f681](https://gitlab.com/wirvsvirus-410/frontend/commit/152f681d6c5081981c6fbf703e6b274abbede9f0))
* legend ([2eaffc4](https://gitlab.com/wirvsvirus-410/frontend/commit/2eaffc4eb9128d60f7204110009588bb1891482e))


### Bug Fixes

* **Headerbar:** menu items over app-logo - screen width of <1500px ([a557f5b](https://gitlab.com/wirvsvirus-410/frontend/commit/a557f5b773a4afd994ca0c7c5c4309183143ea3b))
* **Menu.tsx:** each child should have unique key ([7312c3e](https://gitlab.com/wirvsvirus-410/frontend/commit/7312c3e8b5051f2b3051af377fde4b3689e7b030))
* control-panel height overflow ([1366c62](https://gitlab.com/wirvsvirus-410/frontend/commit/1366c62f3589f10eede0751e73fc3f0a4516b123))
* language ([05cd503](https://gitlab.com/wirvsvirus-410/frontend/commit/05cd5031441c171bb04b902068a1e2fa2c2ae5d0))

### [0.1.2](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1...v0.1.2) (2020-03-22)


### Features

* add favicon ([0f274f6](https://gitlab.com/wirvsvirus-410/frontend/commit/0f274f6d2bf44aea65cc79867e2a3c69a0fcf6d9))
* add slider-ui and adjusted control-panel out of map ([33dff16](https://gitlab.com/wirvsvirus-410/frontend/commit/33dff1661ffb6e1ade5aabdbcf5964ebc6d48d8a))
* changed theme ([0b3ee3f](https://gitlab.com/wirvsvirus-410/frontend/commit/0b3ee3fdf5e01d1409c029491d87177b7a98062e))


### Bug Fixes

* colors and height issues ([4159ebf](https://gitlab.com/wirvsvirus-410/frontend/commit/4159ebf18b200f85eaebf54b4f853e745a052259))
* contro-panel height caluclation ([4e5bc5b](https://gitlab.com/wirvsvirus-410/frontend/commit/4e5bc5b526110af9b2c1d35973e348519fa1ccf6))

### [0.1.1](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1-alpha.6...v0.1.1) (2020-03-22)


### Bug Fixes

* use divi datasource ([847f17d](https://gitlab.com/wirvsvirus-410/frontend/commit/847f17d48b9238ca5955fdc994b0e64fff30cdfe))

### 0.1.1-alpha.6 (2020-03-22)


### Features

* add basic control panel ([6e055e3](https://gitlab.com/wirvsvirus-410/frontend/commit/6e055e37a5b5e469f7c0a99323e60f7fcfea6882))
* add cluster expansion zoom ([72e454e](https://gitlab.com/wirvsvirus-410/frontend/commit/72e454ef9eb5dd7fc57f5e89555b73d7997b2c77))
* add clustering to map ([0a9f88e](https://gitlab.com/wirvsvirus-410/frontend/commit/0a9f88ed8489611f737834b0faf29a354bb8077b))
* add debounce on search input ([2fdcc45](https://gitlab.com/wirvsvirus-410/frontend/commit/2fdcc45960d5b0e13885dfab4c0364384b39f543))
* add green to red color interpolation using cluster average ([fa4147d](https://gitlab.com/wirvsvirus-410/frontend/commit/fa4147d96b1c0e9facc431769b25bff1ec81a796))
* add react-flexbox ([a01d97c](https://gitlab.com/wirvsvirus-410/frontend/commit/a01d97c4700be29bd5c2c9ea73c5e440978a0337))
* add react-icons ([0119f32](https://gitlab.com/wirvsvirus-410/frontend/commit/0119f3211ef28f6ec77eedd045728fcabdcce6bd))
* added app-layout template and theme ([dfd6b1e](https://gitlab.com/wirvsvirus-410/frontend/commit/dfd6b1e000579cbc37941783e02f50d8463c041e))
* added basic control panel and searchbox ([b5a25c7](https://gitlab.com/wirvsvirus-410/frontend/commit/b5a25c7913627a06cb1533b5f78ecbd07d3cd6e2))
* added description ([22c85dc](https://gitlab.com/wirvsvirus-410/frontend/commit/22c85dc3f5e812027bb631ea53cc126191e2ae15))
* added title and favicon ([fd830a2](https://gitlab.com/wirvsvirus-410/frontend/commit/fd830a299eb30e6b684934c6e10ffcd6b0ac23e9))
* changed map colors ([79b57bc](https://gitlab.com/wirvsvirus-410/frontend/commit/79b57bc1a4efe10302656928315f16c646eb439a))
* get coordinates and name of region ([48d3aa7](https://gitlab.com/wirvsvirus-410/frontend/commit/48d3aa757adfbf623839515edb368bfbd569d54e))
* make map fullscreen ([de1132d](https://gitlab.com/wirvsvirus-410/frontend/commit/de1132d49b2672033e0e3ec9ae8735305dd4c21a))
* make map responsive ([11cc9e5](https://gitlab.com/wirvsvirus-410/frontend/commit/11cc9e5d97434dd695adf27c4496275e0c2c917a))
* search box with autocomplete ([1ccbb7b](https://gitlab.com/wirvsvirus-410/frontend/commit/1ccbb7b16f1d02400feadf237b0ff1e3116e80f8))
* set default location and zoom to germany ([7f11bcf](https://gitlab.com/wirvsvirus-410/frontend/commit/7f11bcf9d184899417ef93a78f86fd5f9a728275))


### Bug Fixes

* assertion error on right click ([bc4e7e6](https://gitlab.com/wirvsvirus-410/frontend/commit/bc4e7e629fac39254cc784bc6ef862eb433a8aaf))
* changed colors and layouts ([5672867](https://gitlab.com/wirvsvirus-410/frontend/commit/5672867d5f820ec7cc66685251d310630d623873))
* layout after rebase ([527f68e](https://gitlab.com/wirvsvirus-410/frontend/commit/527f68e5db110af821045e21a7f60f3f10af7c6d))
* unused component ([fd5c3d4](https://gitlab.com/wirvsvirus-410/frontend/commit/fd5c3d43189167011190ab4b178f64720b5c2dae))

### [0.1.1-alpha.5](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1-alpha.4...v0.1.1-alpha.5) (2020-03-22)

### [0.1.1-alpha.4](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1-alpha.3...v0.1.1-alpha.4) (2020-03-22)

### [0.1.1-alpha.3](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1-alpha.2...v0.1.1-alpha.3) (2020-03-21)

### [0.1.1-alpha.2](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1-alpha.1...v0.1.1-alpha.2) (2020-03-21)

### [0.1.1-alpha.1](https://gitlab.com/wirvsvirus-410/frontend/compare/v0.1.1-alpha.0...v0.1.1-alpha.1) (2020-03-21)

### 0.1.1-alpha.0 (2020-03-21)


### Features

* add cluster expansion zoom ([72e454e](https://gitlab.com/wirvsvirus-410/frontend/commit/72e454ef9eb5dd7fc57f5e89555b73d7997b2c77))
* add clustering to map ([0a9f88e](https://gitlab.com/wirvsvirus-410/frontend/commit/0a9f88ed8489611f737834b0faf29a354bb8077b))
* add green to red color interpolation using cluster average ([fa4147d](https://gitlab.com/wirvsvirus-410/frontend/commit/fa4147d96b1c0e9facc431769b25bff1ec81a796))
* add react-flexbox ([a01d97c](https://gitlab.com/wirvsvirus-410/frontend/commit/a01d97c4700be29bd5c2c9ea73c5e440978a0337))
* add react-icons ([0119f32](https://gitlab.com/wirvsvirus-410/frontend/commit/0119f3211ef28f6ec77eedd045728fcabdcce6bd))
* added app-layout template and theme ([dfd6b1e](https://gitlab.com/wirvsvirus-410/frontend/commit/dfd6b1e000579cbc37941783e02f50d8463c041e))
* added title and favicon ([fd830a2](https://gitlab.com/wirvsvirus-410/frontend/commit/fd830a299eb30e6b684934c6e10ffcd6b0ac23e9))
* changed map colors ([79b57bc](https://gitlab.com/wirvsvirus-410/frontend/commit/79b57bc1a4efe10302656928315f16c646eb439a))
* get coordinates and name of region ([48d3aa7](https://gitlab.com/wirvsvirus-410/frontend/commit/48d3aa757adfbf623839515edb368bfbd569d54e))
* make map fullscreen ([de1132d](https://gitlab.com/wirvsvirus-410/frontend/commit/de1132d49b2672033e0e3ec9ae8735305dd4c21a))
* make map responsive ([11cc9e5](https://gitlab.com/wirvsvirus-410/frontend/commit/11cc9e5d97434dd695adf27c4496275e0c2c917a))
* search box with autocomplete ([1ccbb7b](https://gitlab.com/wirvsvirus-410/frontend/commit/1ccbb7b16f1d02400feadf237b0ff1e3116e80f8))
* set default location and zoom to germany ([7f11bcf](https://gitlab.com/wirvsvirus-410/frontend/commit/7f11bcf9d184899417ef93a78f86fd5f9a728275))


### Bug Fixes

* assertion error on right click ([bc4e7e6](https://gitlab.com/wirvsvirus-410/frontend/commit/bc4e7e629fac39254cc784bc6ef862eb433a8aaf))
* layout after rebase ([527f68e](https://gitlab.com/wirvsvirus-410/frontend/commit/527f68e5db110af821045e21a7f60f3f10af7c6d))
