export function debounce<T extends (...args: any[]) => any>(callback: T, time: number): T {
  let interval: NodeJS.Timeout | null
  const result: T = ((...args) => {
    if (interval) {
      clearTimeout(interval)
    }
    interval = setTimeout(() => {
      interval = null

      // eslint-disable-next-line
      callback.apply(null, args)
    }, time)
  }) as T

  return result
}
