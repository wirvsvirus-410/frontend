import React, { FC } from 'react'
import { Headerbar } from '../../components/Headerbar'
import { Footerbar } from '../../components/Footerbar'

import './AppLayout.scss'

export const AppLayout: FC = ({ children }) => {
  return (
    <>
      <Headerbar />
      {children}
      <Footerbar />
    </>
  )
}
