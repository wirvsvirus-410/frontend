import React, { FC } from 'react'

import './Menu.scss'

interface MenuItem {
  label: string
  href: string
}

interface MenuProps {
  items: Array<MenuItem>
}

export const Menu: FC<MenuProps> = ({ items }) => (
  <div className="menu">
    {items.map(({ label }, index) => (
      <div key={index} className="menu--item">
        {label}
      </div>
    ))}
  </div>
)
