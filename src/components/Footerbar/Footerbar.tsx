/* eslint-disable react/jsx-no-target-blank */
import React, { FC } from 'react'
import { Row, Col } from 'react-flexbox-grid'
import { Appbar } from '../../components/Appbar'

import './Footerbar.scss'

interface FooterbarProps {}

const Footer: FC = () => (
  <Row className="footer--bar">
    <Col xs={5} className="label--date">
      Stand der Daten: Samstag, 21.03.2020, 00:00 Uhr
    </Col>
    <Col xs={2} className="label--impressum">
      Impressum | Datenschutz
    </Col>
    <Col xs={5} className="label--disclaimer">
      Prototyp-Anwendung von{' '}
      <a href="https://devpost.com/software/intensive-care-prediction" target="_blank">
        #WirVsVirus
      </a>{' '}
      - Die verwendeten Daten dienen lediglich der Demonstration.
    </Col>
  </Row>
)

export const Footerbar: FC<FooterbarProps> = () => (
  <Appbar style={{ borderTop: '1px solid #ececec' }}>
    <Footer />
  </Appbar>
)
