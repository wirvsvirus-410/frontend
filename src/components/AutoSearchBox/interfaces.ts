export interface Coordinates {
  lat: number
  lng: number
}

export type RegionSelectEvent = (region: string, latLng?: Coordinates) => void
