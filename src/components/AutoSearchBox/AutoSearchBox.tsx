import React, { FC, useState } from 'react'
import cn from 'classnames'
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import { MdSearch, MdClose } from 'react-icons/md'
import { RegionSelectEvent } from './interfaces'

import './AutoSearchBox.scss'
interface SearchBoxProps {
  onChange: RegionSelectEvent
  className?: string
}

const iconProps = {
  size: 24,
  color: 'rgb(144, 144, 144)',
}

const removeCountry: (text: string) => string = (text) => text.replace(', Deutschland', '').trim()

export const AutoSearchBox: FC<SearchBoxProps> = ({ onChange, className }) => {
  const [inputVal, setInputVal] = useState<string>('')

  const handleChange: (value: string) => void = (value) => {
    setInputVal(value)
  }

  const handleSelect: (selectedValue: string, _placeId: string) => void = (selectedValue) => {
    geocodeByAddress(selectedValue)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => onChange(removeCountry(selectedValue), latLng))
      .catch((error) => console.error('Error', error))
  }

  const handleReset: () => void = () => {
    setInputVal('')
    onChange('', undefined)
  }

  return (
    <PlacesAutocomplete
      value={inputVal}
      onChange={handleChange}
      onSelect={handleSelect}
      searchOptions={{
        componentRestrictions: { country: 'de' },
        types: ['(regions)'],
      }}
      debounce={500}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div className={cn('location-search', className)}>
          <span className="location-search__icon-wrapper">
            <MdSearch {...iconProps} />
          </span>
          <span onClick={handleReset} className="location-search__reset-wrapper">
            <MdClose {...iconProps} />
          </span>
          <input
            {...getInputProps({
              placeholder: 'Stadt / Bundesland suchen...',
            })}
          />
          <div className="location-search__dropdown">
            {loading && <div className="location-search__dropdown--suggestion-item">Loading...</div>}
            {suggestions.map((suggestion, i) => (
              <div key={i} {...getSuggestionItemProps(suggestion, {})} className="location-search__dropdown--suggestion-item">
                <span>{suggestion.formattedSuggestion.mainText}</span>
              </div>
            ))}
          </div>
        </div>
      )}
    </PlacesAutocomplete>
  )
}
