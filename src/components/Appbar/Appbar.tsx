import React, { FC, CSSProperties } from 'react'

import './Appbar.scss'

interface AppbarProps {
  color?: 'primary'
  style?: CSSProperties
}

export const Appbar: FC<AppbarProps> = ({ color, style, children }) => {
  return (
    <div className={`appbar color--${color}`} style={style}>
      {children}
    </div>
  )
}
