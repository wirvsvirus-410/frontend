import React, { FC } from 'react'
import { Row, Col } from 'react-flexbox-grid'
import { Menu } from '../../components/Menu'
import { Appbar } from '../../components/Appbar'
import { MENU_ITEMS } from '../../fixtures/MENU_ITEMS'

import Switch from 'react-switch'

interface HeaderbarProps {}

const HeaderMenu: FC = () => (
  <Row middle="xs">
    <Col xs style={{ flex: 0 }}>
      <img className="logo" src={process.env.PUBLIC_URL + '/images/icp-logo-grey.png'} alt="icp-logo" />
    </Col>
    <Col xs>
      <Menu items={MENU_ITEMS} />
    </Col>
    <Col xs className="menu--col--end">
      <Row middle="xs" end="xs">
        <Col style={{ marginRight: 10 }}>Barrierefrei-Modus</Col>
        <Col style={{ marginRight: 40 }}>
          <Switch disabled onChange={() => {}} checked={false} className="react-switch" />
        </Col>

        <Col style={{ marginRight: 10 }}>Nacht-Modus</Col>
        <Col>
          <Switch disabled onChange={() => {}} checked={false} className="react-switch" />
        </Col>
      </Row>
    </Col>
  </Row>
)

export const Headerbar: FC<HeaderbarProps> = () => {
  return (
    <Appbar color="primary">
      <HeaderMenu />
    </Appbar>
  )
}
