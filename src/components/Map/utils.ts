import { AnySourceImpl, GeoJSONSource } from 'mapbox-gl'

import { ifElse, path, pipe } from 'ramda'

const hasProp = (prop: string) => (obj: unknown) => Boolean(path(['current', prop], obj))

const getDimension = (prop: string) =>
  ifElse(
    hasProp(prop),
    pipe(path(['current', prop]), (value?: number) => `${value}px`),
    () => '100%'
  )

export const getElementWidth = getDimension('clientWidth')
export const getElementHeight = getDimension('clientHeight')

export const isGeoJSONSource = (source: AnySourceImpl): source is GeoJSONSource => 'getClusterExpansionZoom' in source
