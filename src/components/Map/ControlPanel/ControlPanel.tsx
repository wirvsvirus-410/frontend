import React, { FC, useState } from 'react'
// import { IoIosArrowBack } from "react-icons/io";
import { Row, Col } from 'react-flexbox-grid'
import Select from 'react-select'
import Switch from 'react-switch'
import 'rc-slider/assets/index.css'
import Slider from 'rc-slider'

import { AutoSearchBox } from '../../../components'
import { useMapView } from '../../../context'

import './ControlPanel.scss'

export enum CapacityType {
  lc,
  hc,
  ecmo,
}

export interface ControlProps {
  showInfections: boolean
  showCapacity: boolean
  capacityType: CapacityType
  predictionDay: number
}

interface ControlPanelProps {
  controlProps: ControlProps
  onChange: (controlProps: ControlProps) => void
}

interface CapacityOptionType {
  value: CapacityType
  label: string
}

const capacityTypes: Array<CapacityOptionType> = [
  { value: CapacityType.lc, label: 'Low care (nicht-invasive Beatmung (NIV), keine Organersatztherapie)' },
  {
    value: CapacityType.hc,
    label: 'High care (invasive Beatmung, Organersatztherapie, vollständige intensivmedizinische Therapiemöglichkeiten)',
  },
  { value: CapacityType.ecmo, label: 'ECMO (Zusätzlich ECMO / Lungenunterstützung)' },
]

export const ControlPanel: FC<ControlPanelProps> = ({ controlProps, onChange }) => {
  const [showCapacity, setShowCapacity] = useState(controlProps.showCapacity)
  const [showInfections, setShowInfections] = useState(controlProps.showInfections)
  const [predictionPeriode, setPredictionPeriode] = useState(controlProps.predictionDay)
  const [capacityType, setCapacityType] = useState<CapacityOptionType>(
    capacityTypes.find((c) => c.value === controlProps.capacityType) || capacityTypes[0]
  )
  const { changeRegion } = useMapView()

  const onChangeShowCapacity = (checked: boolean) => {
    setShowCapacity(checked)
    onChange({
      ...controlProps,
      showCapacity: checked,
    })
  }

  const onChangeShowInfections = (checked: boolean) => {
    setShowInfections(checked)
    onChange({
      ...controlProps,
      showInfections: checked,
    })
  }

  const handlePredictionSlider = (newPredictionDay: number): void => {
    setPredictionPeriode(newPredictionDay)
    onChange({
      ...controlProps,
      predictionDay: newPredictionDay,
    })
  }

  const handleAccuracy = (predictionPeriode: number): JSX.Element => {
    if (predictionPeriode <= 2) {
      return <span style={{ color: '#04a704', fontWeight: 'bold' }}>Hoch</span>
    } else if (predictionPeriode <= 5) {
      return <span style={{ color: '#ffbe00', fontWeight: 'bold' }}>Mittel</span>
    } else {
      return <span style={{ color: '#c10000', fontWeight: 'bold' }}>Gering</span>
    }
  }

  const generateRangeLabels = (maxValue: number): Array<string> => {
    return Array(maxValue + 1)
      .fill(null)
      .map((_, i) => {
        return i === 0 ? 'Heute' : i.toString()
      })
  }

  const handleChangeCapacityType = (capacityType: any) => {
    setCapacityType(capacityType)
    onChange({
      ...controlProps,
      capacityType: capacityType.value,
    })
  }

  return (
    <div className="control-panel">
      <AutoSearchBox onChange={changeRegion} className="search-box" />
      {/* <button className="collapser">
        <IoIosArrowBack />
      </button> */}
      <hr className="divider" />
      <Row>
        <Col xs={12}>
          <h3>Covid-19 Infektionen</h3>
        </Col>
      </Row>
      <Row middle="xs">
        <Col md={6}>
          <Row middle="xs" between="xs" className="control-panel-row">
            <Col>Auf Karte zeigen</Col>
            <Col>
              <Switch onChange={onChangeShowInfections} checked={showInfections} className="react-switch" />
            </Col>
          </Row>
        </Col>
      </Row>

      <hr className="divider" />

      <Row>
        <Col xs={12}>
          <h3>Auslastung Intensivbetten</h3>
        </Col>
      </Row>
      <Row middle="xs">
        <Col md={6}>
          <Row middle="xs" between="xs" className="control-panel-row">
            <Col>Auf Karte zeigen </Col>
            <Col>
              <Switch onChange={onChangeShowCapacity} checked={showCapacity} className="react-switch" />
            </Col>
          </Row>
        </Col>
      </Row>

      <Row style={{ marginTop: 20 }}>
        <Col xs={12}>
          <Select value={capacityType} onChange={handleChangeCapacityType} options={capacityTypes} />
        </Col>
      </Row>

      <hr className="divider" />

      <Row className="prediction-label">
        <Col xs={6}>
          <h3>Vorhersage (in Tagen)</h3>
        </Col>
        <Col xs={6} style={{ textAlign: 'right' }}>
          Genauigkeit: {handleAccuracy(predictionPeriode)}
        </Col>
      </Row>
      <Row className="prediction-slider">
        <Col xs={12}>
          <Slider
            value={predictionPeriode}
            onChange={handlePredictionSlider}
            step={1}
            dots
            max={9}
            marks={generateRangeLabels(9)}
          />
        </Col>
      </Row>

      <hr className="divider" />

      <Row>
        <Col xs={12}>
          <h3>Legende</h3>
        </Col>
      </Row>
      <Row>
        <Col xs={3}>
          <div className="colorpatch colorpatch--covid"></div>
        </Col>
        <Col xs={9}>Covid-19 Gesamtinfektionen</Col>
      </Row>
      <Row style={{ marginTop: 10 }}>
        <Col xs={3}>
          <div className="cp--ic cp--ic--good"></div>
          <div className="cp--ic cp--ic--medium"></div>
          <div className="cp--ic cp--ic--critical"></div>
        </Col>
        <Col xs={9}>Intensivbetten Auslastung</Col>
      </Row>
    </div>
  )
}
