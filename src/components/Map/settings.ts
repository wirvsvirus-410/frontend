import { LayerProps } from 'react-map-gl'
import { Expression } from 'mapbox-gl'

import { CapacityType } from './ControlPanel/ControlPanel'

export const GERMANY_LAT_LONG = {
  latitude: 51.133481,
  longitude: 10.018343,
}

export const DEFAULT_ZOOM = 6

export const CAPACITY_SOURCE_ID = 'capacitySource'
export const INFECTION_SOURCE_ID = 'infectionSource'

export const CAPACITY_CLUSTER_ID = 'capacityCluster'
export const INFECTION_CLUSTER_ID = 'infectionCluster'

export const MAPBOX_TOKEN = 'pk.eyJ1IjoiamFza29tZSIsImEiOiJjazgxZzF1OGYwYWloM29tdzhnM29xaTN2In0.gBFtz_PE__-fx-ZefcJvrw'

const Green: Expression = ['rgba', 58, 228, 75, 0.51]
const Yellow: Expression = ['rgba', 228, 211, 58, 0.51]
const Red: Expression = ['rgba', 228, 83, 58, 0.51]

const Purple: Expression = ['rgba', 170, 132, 177, 0.2]
const DarkGrey: Expression = ['rgba', 92, 92, 92, 1]

const MapColors = {
  Green,
  Yellow,
  Red,
  Purple,
  DarkGrey,
}

interface ClusterProps {
  [key: string]: Expression
}

interface SourceSettings {
  clusterLayer: LayerProps
  clusterProperties: ClusterProps
  pointLayer: LayerProps
  labelLayer?: LayerProps
}

const capacityTypeMap = {
  [CapacityType.lc]: 'status_icuLowCare',
  [CapacityType.hc]: 'status_icuHighCare',
  [CapacityType.ecmo]: 'status_ecmo',
}

export const getCapacitySettings: (capacityType: CapacityType) => SourceSettings = (capacityType) => {
  const capacityTypeProp: Expression = ['get', capacityTypeMap[capacityType]]
  const clusterProperties: ClusterProps = {
    sumLevel: ['+', capacityTypeProp],
  }

  const averageLevelProp = ['/', ['get', 'sumLevel'], ['get', 'point_count']]

  const clusterLayer: LayerProps = {
    id: CAPACITY_CLUSTER_ID,
    type: 'circle',
    source: 'capacity',
    filter: ['has', 'point_count'],
    paint: {
      'circle-color': [
        'interpolate-hcl',
        ['linear'],
        averageLevelProp,
        1,
        MapColors.Green,
        2,
        MapColors.Yellow,
        3,
        MapColors.Red,
      ],
      'circle-radius': ['step', averageLevelProp, 20, 2, 25, 3, 30],
    },
  }

  const pointLayer: LayerProps = {
    id: 'capacityPoints',
    type: 'circle',
    source: 'capacity',
    filter: ['all', ['!has', 'point_count'], ['>', capacityTypeMap[capacityType], 0]],
    paint: {
      'circle-color': ['step', capacityTypeProp, MapColors.Green, 2, MapColors.Yellow, 3, MapColors.Red],
      'circle-radius': 12.5,
    },
  }

  const labelLayer: LayerProps = {
    id: 'capacityLabels',
    type: 'symbol',
    source: 'capacity',
    filter: ['!has', 'point_count'],
    minzoom: 7,
    paint: {
      'text-color': MapColors.DarkGrey,
    },
    layout: {
      'text-field': '{hospitalName}',
      'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
      'text-size': 12,
    },
  }

  return {
    clusterProperties,
    clusterLayer,
    pointLayer,
    labelLayer,
  }
}

export const getInfectionSettings: (day: number) => SourceSettings = (day) => {
  const forcastProp: Expression = ['at', day, ['get', 'forcast']]
  const averageProp: Expression = ['/', ['get', 'sum'], ['get', 'point_count']]
  const clusterProperties: ClusterProps = {
    sum: ['+', forcastProp],
  }

  const clusterLayer: LayerProps = {
    id: INFECTION_CLUSTER_ID,
    type: 'circle',
    source: 'predictions',
    filter: ['has', 'point_count'],
    paint: {
      'circle-color': MapColors.Purple,
      'circle-radius': ['interpolate', ['linear'], averageProp, 0, 5, 1, 40],
    },
  }

  const pointLayer: LayerProps = {
    id: 'predictionPoints',
    type: 'circle',
    source: 'predictions',
    filter: ['!', ['has', 'point_count']],
    paint: {
      'circle-color': MapColors.Purple,
      'circle-radius': ['interpolate', ['linear'], forcastProp, 0, 5, 1, 40],
    },
  }

  return {
    clusterProperties,
    clusterLayer,
    pointLayer,
  }
}
