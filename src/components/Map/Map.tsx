import React, { FC, useEffect, useCallback, useRef, useState } from 'react'
import ReactMapGL, { Source, Layer, InteractiveMapProps, PointerEvent } from 'react-map-gl'
import { Map as MapType } from 'mapbox-gl'
import { FeatureCollection } from 'geojson'
import { useMapView } from '../../context'

import { debounce } from '../../utils/debounce'
import {
  DEFAULT_ZOOM,
  GERMANY_LAT_LONG,
  MAPBOX_TOKEN,
  CAPACITY_SOURCE_ID,
  INFECTION_SOURCE_ID,
  CAPACITY_CLUSTER_ID,
  INFECTION_CLUSTER_ID,
  getCapacitySettings,
  getInfectionSettings,
} from './settings'
import { getElementWidth, getElementHeight, isGeoJSONSource } from './utils'

import { ControlPanel, ControlProps, CapacityType } from './ControlPanel/ControlPanel'

import diviData from './__fixtures__/divi.json'
import predictionData from './__fixtures__/prediction.json'
import './Map.scss'

/**
 * Displays fullscreen map from react-map-gl.
 */
export const Map: FC = () => {
  const [controlState, setControlState] = useState<ControlProps>({
    showInfections: true,
    showCapacity: true,
    capacityType: CapacityType.lc,
    predictionDay: 0,
  })
  const capacityRef = useRef<MapType>(null)
  const infectionsRef = useRef<MapType>(null)
  const containerRef = useRef(null)
  const { latLng } = useMapView()
  const [viewport, setViewport] = useState<InteractiveMapProps>({
    width: '100%',
    height: '100%',
    zoom: DEFAULT_ZOOM,
    ...GERMANY_LAT_LONG,
  })

  useEffect(() => {
    latLng && setViewport((viewp) => ({ ...viewp, zoom: 10, latitude: latLng?.lat, longitude: latLng?.lng }))
  }, [latLng])

  useEffect(() => {
    const resizeListener = debounce(() => {
      setViewport({
        ...viewport,
        width: getElementWidth(containerRef),
        height: getElementHeight(containerRef),
      })
    }, 100)

    window.addEventListener('resize', resizeListener)

    return () => {
      window.removeEventListener('resize', resizeListener)
    }
  }, [viewport, containerRef])

  const onClick = useCallback(
    (event: PointerEvent) => {
      if (event.features && event.features.length) {
        const feature = event.features[0]
        const clusterId = feature.properties.cluster_id
        const sourceRef = feature.layer.id === CAPACITY_CLUSTER_ID ? capacityRef : infectionsRef
        const sourceId = feature.layer.id === CAPACITY_CLUSTER_ID ? CAPACITY_SOURCE_ID : INFECTION_SOURCE_ID

        if (sourceRef.current) {
          const mapboxSource = sourceRef.current.getSource(sourceId)

          if (isGeoJSONSource(mapboxSource)) {
            mapboxSource.getClusterExpansionZoom(clusterId, (err: any, zoom: number) => {
              if (err) {
                return
              }

              setViewport({
                ...viewport,
                longitude: feature.geometry.coordinates[0],
                latitude: feature.geometry.coordinates[1],
                zoom,
                transitionDuration: 500,
              })
            })
          }
        }
      }
    },
    [viewport]
  )

  const capacitySettings = getCapacitySettings(controlState.capacityType)
  const infectionSettings = getInfectionSettings(controlState.predictionDay)

  return (
    <div ref={containerRef} className="mapContainer">
      <ReactMapGL
        {...viewport}
        onViewportChange={setViewport}
        mapboxApiAccessToken={MAPBOX_TOKEN}
        interactiveLayerIds={[CAPACITY_CLUSTER_ID, INFECTION_CLUSTER_ID!]}
        onClick={onClick}
      >
        {controlState.showCapacity && (
          <Source
            id={CAPACITY_SOURCE_ID}
            type="geojson"
            data={diviData as FeatureCollection}
            cluster={true}
            clusterMaxZoom={12}
            clusterRadius={40}
            clusterProperties={capacitySettings.clusterProperties}
            ref={capacityRef as any}
          >
            <Layer {...capacitySettings.clusterLayer} />
            <Layer {...capacitySettings.pointLayer} />
            {!!capacitySettings.labelLayer && <Layer {...capacitySettings.labelLayer} />}
          </Source>
        )}
        {controlState.showInfections && (
          <Source
            id={INFECTION_SOURCE_ID}
            type="geojson"
            data={predictionData as FeatureCollection}
            cluster={true}
            clusterMaxZoom={12}
            clusterRadius={30}
            clusterProperties={infectionSettings.clusterProperties}
            ref={infectionsRef as any}
          >
            <Layer {...infectionSettings.clusterLayer} />
            <Layer {...infectionSettings.pointLayer} />
          </Source>
        )}
      </ReactMapGL>
      <ControlPanel controlProps={controlState} onChange={setControlState} />
    </div>
  )
}
