import React from 'react'
import './App.scss'

import { AppLayout } from './layout/AppLayout'
import { MapViewProvider } from './context'

import { Map } from './components/Map'

function App() {
  return (
    <MapViewProvider>
      <AppLayout>
        <Map />
      </AppLayout>
    </MapViewProvider>
  )
}

export default App
