import React, { FC, useState, createContext, useContext } from 'react'

import { Coordinates, RegionSelectEvent } from '../components/AutoSearchBox/interfaces'

interface MapViewProps {
  latLng?: Coordinates
  region: string
  changeRegion: RegionSelectEvent
}

const INITIAL_MAP_VIEW: MapViewProps = {
  region: '',
  changeRegion: (_region) => {},
}

const MapViewContext = createContext<MapViewProps>(INITIAL_MAP_VIEW)

export const MapViewProvider: FC = ({ children }) => {
  const [region, setRegion] = useState<MapViewProps['region']>(INITIAL_MAP_VIEW.region)
  const [latLng, setLatLng] = useState<MapViewProps['latLng']>(INITIAL_MAP_VIEW.latLng)

  const changeRegion: RegionSelectEvent = (region, latLng) => {
    setRegion(region)
    latLng && setLatLng(latLng)
  }

  return <MapViewContext.Provider value={{ region, latLng, changeRegion }}>{children}</MapViewContext.Provider>
}

export const useMapView: () => MapViewProps = () => {
  const mapViewData = useContext(MapViewContext)
  if (!mapViewData) console.error('useMapView hat to be inner MapViewProvider!')
  return mapViewData
}
